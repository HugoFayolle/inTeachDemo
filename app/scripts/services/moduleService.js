'use strict';

angular.module('inTeachDemoApp')
  .factory('ModuleService', function(localStorageService, globalConst) {
    var functions = {};

    functions.getAllModules = function() {
      var savedModules = localStorageService.get('modules');
      var modules = (localStorageService.get('modules')!==null && typeof JSON.parse(localStorageService.get('modules')) === 'object')
                    ? JSON.parse(savedModules) : globalConst.DEFAULT_MODULES;

      localStorageService.set('modules', JSON.stringify(modules));

      return JSON.parse(localStorageService.get('modules'));
    };

    functions.addModule = function(moduleTitle, moduleDescription) {
      var newModules = JSON.parse(localStorageService.get('modules'));
      newModules.push({
        title: moduleTitle,
        description: moduleDescription,
        courses: []
      });

      localStorageService.set('modules', JSON.stringify(newModules));

      return JSON.parse(localStorageService.get('modules'));
    };

    functions.removeModule = function(moduleIndex) {
      var newModules = JSON.parse(localStorageService.get('modules'));
      newModules.splice(moduleIndex, 1);

      localStorageService.set('modules', JSON.stringify(newModules));

      return JSON.parse(localStorageService.get('modules'));
    };

    functions.editModuleTitle = function(newModuleTitle, moduleID) {
      var savedModules = localStorageService.get('modules');
      var modules = (localStorageService.get('modules')!==null && typeof JSON.parse(localStorageService.get('modules')) === 'object')
        ? JSON.parse(savedModules) : globalConst.DEFAULT_MODULES;

      modules[moduleID].title = newModuleTitle;

      localStorageService.set('modules', JSON.stringify(modules));

      return modules[moduleID];
    };

    functions.editModuleDesc = function(newModuleDesc, moduleID) {
      var savedModules = localStorageService.get('modules');
      var modules = (localStorageService.get('modules')!==null && typeof JSON.parse(localStorageService.get('modules')) === 'object')
        ? JSON.parse(savedModules) : globalConst.DEFAULT_MODULES;

      modules[moduleID].description = newModuleDesc;

      localStorageService.set('modules', JSON.stringify(modules));

      return modules[moduleID];
    };

    functions.updateModules = function(newCourseTitle, moduleID, courseIndex) {
      var savedModules = localStorageService.get('modules');
      var modules = (localStorageService.get('modules')!==null && typeof JSON.parse(localStorageService.get('modules')) === 'object')
        ? JSON.parse(savedModules) : globalConst.DEFAULT_MODULES;

      modules[moduleID].courses[courseIndex].courseTitle = newCourseTitle;

      localStorageService.set('modules', JSON.stringify(modules));

      return modules[moduleID];
    };

    functions.addCourseToModule = function(courseTitle, moduleID) {
      var modules = JSON.parse(localStorageService.get('modules'));

      modules[moduleID].courses.push({
        courseTitle: courseTitle,
        questions: []
      });

      localStorageService.set('modules', JSON.stringify(modules));

      return modules[moduleID];
    };

    functions.removeCourse = function(courseIndex, moduleID) {
      var modules = JSON.parse(localStorageService.get('modules'));

      modules[moduleID].courses.splice(courseIndex, 1);

      localStorageService.set('modules', JSON.stringify(modules));

      return modules[moduleID];
    };

    functions.addQuestionToCourse = function(newQuestion, moduleID, courseIndex) {
      var modules = JSON.parse(localStorageService.get('modules'));

      modules[moduleID].courses[courseIndex].questions.push({
        label: newQuestion,
        answers: []
      });

      localStorageService.set('modules', JSON.stringify(modules));

      return modules[moduleID];
    };

    functions.removeQuestion = function(questionIndex, moduleID, courseIndex) {
      var modules = JSON.parse(localStorageService.get('modules'));

      modules[moduleID].courses[courseIndex].questions.splice(questionIndex, 1);

      localStorageService.set('modules', JSON.stringify(modules));

      return modules[moduleID];
    };

    functions.editQuestionLabel = function(newQuestionLabel, moduleID, courseIndex, questionIndex) {
      var modules = JSON.parse(localStorageService.get('modules'));

      modules[moduleID].courses[courseIndex].questions[questionIndex].label = newQuestionLabel;

      localStorageService.set('modules', JSON.stringify(modules));

      return modules[moduleID];
    };

    functions.addAnswerToQuestion = function(newAnswer, moduleID, courseIndex, questionIndex, answerValue) {
      var modules = JSON.parse(localStorageService.get('modules'));

      modules[moduleID].courses[courseIndex].questions[questionIndex].answers.push({
        label: newAnswer,
        value: answerValue
      });

      localStorageService.set('modules', JSON.stringify(modules));

      return modules[moduleID];
    };

    functions.removeAnswer = function(questionIndex, answerIndex, moduleID, courseIndex) {
      var modules = JSON.parse(localStorageService.get('modules'));

      modules[moduleID].courses[courseIndex].questions[questionIndex].answers.splice(answerIndex, 1);

      localStorageService.set('modules', JSON.stringify(modules));

      return modules[moduleID];
    };

    functions.editAnswerLabel = function(newAnswerLabel, newAnswerValue, moduleID, courseIndex, questionIndex, answerIndex) {
      var modules = JSON.parse(localStorageService.get('modules'));

      modules[moduleID].courses[courseIndex].questions[questionIndex].answers[answerIndex].label = newAnswerLabel;
      modules[moduleID].courses[courseIndex].questions[questionIndex].answers[answerIndex].value = newAnswerValue;

      localStorageService.set('modules', JSON.stringify(modules));

      return modules[moduleID];
    };

    return functions;
  });