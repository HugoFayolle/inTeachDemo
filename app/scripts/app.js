'use strict';

/**
 * @ngdoc overview
 * @name inTeachDemoApp
 * @description
 * # inTeachDemoApp
 *
 * Main module of the application.
 */
angular
  .module('inTeachDemoApp', [
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch',
    'ngLodash',
    'LocalStorageModule'
  ])
  .config(function ($routeProvider, localStorageServiceProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl',
        controllerAs: 'main'
      })
      .when('/module/:id', {
        templateUrl: 'views/module.html',
        controller: 'ModuleCtrl',
        controllerAs: 'module'
      })
      .otherwise({
        redirectTo: '/'
      });

    localStorageServiceProvider
      .setPrefix('inTeachDemoApp')
      .setStorageType('sessionStorage')
      .setDefaultToCookie(false);
  });
