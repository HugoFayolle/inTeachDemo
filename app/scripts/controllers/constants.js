'use strict';

angular.module('inTeachDemoApp')
  .constant('globalConst', {

    'DEFAULT_MODULES': [
      {
        title: 'Template module 1',
        description: 'Description du module test 1',
        type: 'Sciences',
        courses: [
          {
            courseTitle: 'Titre de la leçon numéro 1 du module 1',
            questions: [
              {
                label: 'Intitulé de la question 1 du module 1 - leçon 1',
                answers: [
                  {
                    label: 'Réponse 1 à la question 1 du module 1 - leçon 1',
                    value: true
                  },
                  {
                    label: 'Réponse 2 à la question 1 du module 1 - leçon 1',
                    value: false
                  },
                  {
                    label: 'Réponse 3 à la question 1 du module 1 - leçon 1',
                    value: true
                  }
                ]
              },
              {
                label: 'Intitulé de la question 2 du module 1 - leçon 1',
                answers: [
                  {
                    label: 'Réponse 1 à la question 2 du module 1 - leçon 1',
                    value: false
                  },
                  {
                    label: 'Réponse 2 à la question 2 du module 1 - leçon 1',
                    value: false
                  },
                  {
                    label: 'Réponse 3 à la question 2 du module 1 - leçon 1',
                    value: true
                  },
                  {
                    label: 'Réponse 4 à la question 2 du module 1 - leçon 1',
                    value: true
                  }
                ]
              }
            ]
          },
          {
            courseTitle: 'Titre de la leçon numéro 2 du module 1',
            questions: [
              {
                label: 'Intitulé de la question 1 du module 1 - leçon 2',
                answers: [
                  {
                    label: 'Réponse 1 à la question 1 du module 1 - leçon 2',
                    value: true
                  },
                  {
                    label: 'Réponse 2 à la question 1 du module 1 - leçon 2',
                    value: false
                  },
                  {
                    label: 'Réponse 3 à la question 1 du module 1 - leçon 2',
                    value: true
                  }
                ]
              },
              {
                label: 'Intitulé de la question 2 du module 1 - leçon 2',
                answers: [
                  {
                    label: 'Réponse 1 à la question 2 du module 1 - leçon 2',
                    value: false
                  },
                  {
                    label: 'Réponse 2 à la question 2 du module 1 - leçon 2',
                    value: false
                  },
                  {
                    label: 'Réponse 3 à la question 2 du module 1 - leçon 2',
                    value: true
                  },
                  {
                    label: 'Réponse 4 à la question 2 du module 1 - leçon 2',
                    value: true
                  }
                ]
              }
            ]
          },
          {
            courseTitle: 'Titre de la leçon numéro 3 du module 1',
            questions: [
              {
                label: 'Intitulé de la question 1 du module 1 - leçon 3',
                answers: [
                  {
                    label: 'Réponse 1 à la question 1 du module 1 - leçon 3',
                    value: true
                  },
                  {
                    label: 'Réponse 2 à la question 1 du module 1 - leçon 3',
                    value: false
                  },
                  {
                    label: 'Réponse 3 à la question 1 du module 1 - leçon 3',
                    value: true
                  }
                ]
              },
              {
                label: 'Intitulé de la question 2 du module 1 - leçon 3',
                answers: [
                  {
                    label: 'Réponse 1 à la question 2 du module 1 - leçon 3',
                    value: false
                  },
                  {
                    label: 'Réponse 2 à la question 2 du module 1 - leçon 3',
                    value: false
                  },
                  {
                    label: 'Réponse 3 à la question 2 du module 1 - leçon 3',
                    value: true
                  },
                  {
                    label: 'Réponse 4 à la question 2 du module 1 - leçon 3',
                    value: true
                  }
                ]
              }
            ]
          }
        ]
      },
      {
        title: 'Template module 2',
        description: 'Description du module test 2',
        type: 'Sécurité',
        courses: [
          {
            courseTitle: 'Titre de la leçon numéro 1 du module 2',
            questions: [
              {
                label: 'Intitulé de la question 1 du module 2',
                answers: [
                  {
                    label: 'Réponse 1 à la question 1 du module 2',
                    value: false
                  },
                  {
                    label: 'Réponse 2 à la question 1 du module 2',
                    value: false
                  },
                  {
                    label: 'Réponse 3 à la question 1 du module 2',
                    value: true
                  },
                  {
                    label: 'Réponse 4 à la question 1 du module 2',
                    value: true
                  }
                ]
              }
            ]
          }
        ]
      },
      {
        title: 'Template module 3',
        description: 'Description du module test 3',
        type: 'Culture générale',
        courses: [
          {
            courseTitle: 'Titre de la leçon numéro 1 du module 3',
            questions: [
              {
                label: 'Intitulé de la question 1 du module 3',
                answers: [
                  {
                    label: 'Réponse 1 à la question 1 du module 3',
                    value: true
                  },
                  {
                    label: 'Réponse 2 à la question 1 du module 3',
                    value: false
                  }
                ]
              }
            ]
          }
        ]
      },
      {
        title: 'Template module 4',
        description: 'Description du module test 4',
        type: 'Sécurité',
        courses: [
          {
            courseTitle: 'Titre de la leçon numéro 1 du module 4',
            questions: [
              {
                label: 'Intitulé de la question 1 du module 4',
                answers: [
                  {
                    label: 'Réponse 1 à la question 1 du module 4',
                    value: true
                  },
                  {
                    label: 'Réponse 2 à la question 1 du module 4',
                    value: false
                  },
                  {
                    label: 'Réponse 3 à la question 1 du module 4',
                    value: false
                  },
                  {
                    label: 'Réponse 4 à la question 1 du module 4',
                    value: true
                  }
                ]
              }
            ]
          }
        ]
      },
      {
        title: 'Template module 5',
        description: 'Description du module test 5',
        type: 'Sciences',
        courses: [
          {
            courseTitle: 'Titre de la leçon numéro 1 du module 5',
            questions: [
              {
                label: 'Intitulé de la question 1 du module 5',
                answers: [
                  {
                    label: 'Réponse 1 à la question 1 du module 5',
                    value: false
                  },
                  {
                    label: 'Réponse 2 à la question 1 du module 5',
                    value: true
                  }
                ]
              },
              {
                label: 'Intitulé de la question 2 du module 5',
                answers: [
                  {
                    label: 'Réponse 1 à la question 2 du module 5',
                    value: false
                  },
                  {
                    label: 'Réponse 2 à la question 2 du module 5',
                    value: true
                  },
                  {
                    label: 'Réponse 3 à la question 2 du module 5',
                    value: false
                  }
                ]
              }
            ]
          }
        ]
      }
    ]
  });