'use strict';


/**
 * @ngdoc function
 * @name inTeachDemoApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the inTeachDemoApp
 */
angular.module('inTeachDemoApp')

  .controller('MainCtrl', function ($scope, globalConst, localStorageService, ModuleService) {

    $scope.modules = ModuleService.getAllModules();

    $scope.addModule = function() {
      $scope.modules = ModuleService.addModule($scope.moduleTitle, $scope.moduleDescription);

      $scope.moduleTitle = '';
      $scope.moduleDescription = '';
    };

    $scope.removeModule = function(moduleIndex) {
      $scope.modules = ModuleService.removeModule(moduleIndex);
    };

  });
