'use strict';

angular.module('inTeachDemoApp')
  .controller('ModuleCtrl', function($scope, $routeParams, globalConst, lodash, localStorageService, ModuleService) {

    $scope.selectedCourse = 0;
    $scope.newCourseTitle = '';

    var moduleID = $routeParams.id;

    $scope.savedModules = localStorageService.get('modules');
    $scope.modules = (localStorageService.get('modules')!==null) ? JSON.parse($scope.savedModules) : globalConst.DEFAULT_MODULES;
    $scope.selectedModule = $scope.modules[parseInt(moduleID)];

    $scope.changeSelectedCourse = function(courseId) {
      $scope.selectedCourse = courseId;
    };

    $scope.editModuleTitle = function(moduleTitle) {
      var newModuleTitle = prompt('Nouveau titre du module : ', moduleTitle);

      if (newModuleTitle !== '' && newModuleTitle !== null) {
        $scope.selectedModule = ModuleService.editModuleTitle(newModuleTitle, moduleID);
      }


    };

    $scope.editModuleDesc = function(moduleDesc) {
      var newModuleDesc = prompt('Nouvelle description du module : ', moduleDesc);

      if (newModuleDesc !== '' && newModuleDesc !== null) {
        $scope.selectedModule = ModuleService.editModuleDesc(newModuleDesc, moduleID);
      }

    };

    $scope.setPreviousCourseTitleValue = function(courseTitle, courseId) {
      $scope.newCourseTitle = courseTitle;
      $scope.newCourseID = courseId;
    };

    $scope.addCourse = function() {
      var courseTitle = prompt('Titre de la leçon à créer : ');

      if (courseTitle !== '' && courseTitle !== null) {
        $scope.selectedModule = ModuleService.addCourseToModule(courseTitle, moduleID);
      }

    };

    $scope.editCourseTitle = function(courseTitle) {
      var newTitle = prompt('Nouveau titre de la leçon : ', courseTitle);

      if (newTitle !== '' && newTitle !== null) {
        $scope.selectedModule = ModuleService.updateModules(newTitle, moduleID, $scope.selectedCourse);
      }
    };

    $scope.removeCourse = function() {
      if (confirm('Êtes-vous sûr de vouloir supprimer ce cours ?')) {
        $scope.selectedModule = ModuleService.removeCourse($scope.selectedCourse, moduleID);

        $scope.selectedCourse = 0;
      }
    };

    $scope.addQuestion = function() {
      var newQuestion = prompt('Intitulé de la question à insérer : ');

      if (newQuestion !== '' && newQuestion !== null) {
        $scope.selectedModule = ModuleService.addQuestionToCourse(newQuestion, moduleID, $scope.selectedCourse);
      }
    };

    $scope.removeQuestion = function(questionIndex) {
      if (confirm('Êtes-vous sûr de vouloir supprimer cette question ?')) {
        $scope.selectedModule = ModuleService.removeQuestion(questionIndex, moduleID, $scope.selectedCourse);
      }
    };

    $scope.editQuestion = function(questionLabel, questionIndex) {
      var newQuestionLabel = prompt('Nouvel intitulé de la question : ', questionLabel);

      if (newQuestionLabel !== '' && newQuestionLabel !== null) {
        $scope.selectedModule = ModuleService.editQuestionLabel(
          newQuestionLabel, moduleID, $scope.selectedCourse, questionIndex);
      }
    };

    $scope.addAnswer = function(questionIndex) {
      var newAnswer = prompt('Intitulé de la réponse à insérer : ');

      if (newAnswer !== '' && newAnswer !== null) {

        var answerValue = confirm('Voulez-vous que la réponse soit juste ?');
        $scope.selectedModule = ModuleService.addAnswerToQuestion(newAnswer, moduleID, $scope.selectedCourse,
            questionIndex, answerValue);
      }
    };

    $scope.removeAnswer = function(questionIndex, answerIndex) {
      if (confirm('Êtes-vous sûr de vouloir supprimer cette réponse ?')) {
        $scope.selectedModule = ModuleService.removeAnswer(questionIndex, answerIndex, moduleID, $scope.selectedCourse);
      }
    };

    $scope.editAnswer = function(answerLabel, questionIndex, answerIndex) {
      var newAnswerLabel = prompt('Nouvel intitulé de la réponse : ', answerLabel);

      if (newAnswerLabel !== '' && newAnswerLabel !== null) {
        var newAnswerValue = confirm('Voulez-vous que la réponse soit juste ?');
        $scope.selectedModule = ModuleService.editAnswerLabel(newAnswerLabel, newAnswerValue, moduleID,
          $scope.selectedCourse, questionIndex, answerIndex);
      }
    };

    $scope.displayAnswerValue = function(answerValue) {
      return answerValue ? 'Vrai' : 'Faux'
    }
  });